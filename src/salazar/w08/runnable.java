package salazar.w08;

import java.util.Random;

public class runnable implements Runnable{
    private String name;
    private int number;


    public runnable (String name) {
        this.name = name;
        Random random = new Random();
        this.number = random.nextInt(50);
    }
    public void run() {
        int x = 1;
        System.out.print(name + "  has started. \n");
        while (x <= number) {
            System.out.print(name + "  is running. \n");
            x++;
        }
        System.out.print(name + "  has finished. \n");
    }

}
