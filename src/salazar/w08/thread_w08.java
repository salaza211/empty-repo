package salazar.w08;


import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class thread_w08 extends Thread{
    private String name;
    private int number;
    static final AtomicInteger count = new AtomicInteger(0);


    public thread_w08 (String name) {
        this.name = name;
        Random random = new Random();
        this.number = random.nextInt(50);
    }

    public void run() {
        int x = 1;
        System.out.print(name + "  has started. \n");
        System.out.print("The amount of times for " + name + " to be run is " + number + "\n");
        while (x <= number ) {
            System.out.print(name + "  is running. \n");
            x++;
            count.addAndGet(1);
        }
        System.out.print(name + "  has finished. \n");
    }

}
