package salazar.w08;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EXECUTE_W08 {
    public static void main(String[] args) throws InterruptedException {
        System.out.print("We start by executing a THREAD CLASS. \n");
        System.out.print("We will have an atomic number that will keep track of. \n");
        System.out.print("the amounts of times that each class wil be ran. \n");
        System.out.print("-------------------------------------------- \n");


        thread_w08 thread_1 = new thread_w08("Sam_1");
        thread_w08 thread_2 = new thread_w08("Rudy_1");
        thread_w08 thread_3 = new thread_w08("Frodo_1");
        thread_1.start();
        thread_2.start();
        thread_3.start();

        try {
            thread_1.join();
            thread_2.join();
            thread_3.join();
        } catch (InterruptedException e) {
            System.out.print("There was a problem when joining all threads. ");
        }
        System.out.print("Our count is " + thread_3.count + " \n");

        System.out.print(" \n");
        System.out.print(" \n");

        System.out.print("We then execute a RUNNABLE CLASS. \n");
        System.out.print("-------------------------------------------- \n");
        runnable run_1 = new runnable("Sam_2");
        runnable run_2 = new runnable("Rudy_2");
        runnable run_3 = new runnable("Frodo_2");
        run_1.run();
        run_2.run();
        run_3.run();


        System.out.print(" \n");
        System.out.print(" \n");

        System.out.print("Finally, we use an EXECUTOR to limit the number threads while executing the previous class. \n");
        System.out.print("-------------------------------------------- \n");
        runnable run_4 = new runnable("Sam_3");
        runnable run_5 = new runnable("Rudy_3");
        runnable run_6 = new runnable("Frodo_3");
        ExecutorService exec_1 = Executors.newFixedThreadPool(2);
        exec_1.execute(run_4);
        exec_1.execute(run_5);
        exec_1.execute(run_6);

        exec_1.shutdown();
    }
}
