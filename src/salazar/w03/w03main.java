package salazar.w03;
import java.util.*;

public class w03main {
    public static void main(String[] args) throws Exception {

        Scanner in = new Scanner(System.in);
        double number1;
        double number2;
        boolean next = false;
        double num = 0;
        do {
            System.out.println("Please enter the first number: ");
            number1 = in.nextInt();
            System.out.println("Please enter the second number: ");
            number2 = in.nextInt();
            try {
                num = division(number1, number2);
                next = true;
            } catch (ArithmeticException e) {
                System.out.println("You should not divide a number by zero.");
                System.out.println("Enter the numbers again.");
            }
        } while (!next);
        System.out.println("The division is: " + num);


    }

    public static double division (double n1, double n2) throws ArithmeticException {

        double number = n1/n2;
        if (n2 == 0.0) {
            throw new ArithmeticException();
        }
        return number;
        }

}
