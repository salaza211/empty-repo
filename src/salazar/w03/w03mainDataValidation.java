package salazar.w03;
import java.util.*;

public class w03mainDataValidation {

    public static void main(String[] args) throws Exception {
        boolean next = false;
        double number1 = 0;
        double number2 = 0;
        double num = 0;
        do {
            try {
                number1 = dataValidation1();
                number2 = dataValidation2();
            } catch (Exception e) {
                System.out.println("You should only enter numbers.");
                System.out.println("Enter the numbers again.");
                continue;
            }
            try {
                num = division2(number1, number2);
                next = true;
            } catch (ArithmeticException e) {
                System.out.println("You should not divide a number by zero.");
                System.out.println("Enter the numbers again.");
            }
        } while (!next);
        System.out.println("The division is: " + num);


    }
    public static double dataValidation1() {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter the first number: ");
        double n1 = in.nextInt();
        return n1;
    }
    public static double dataValidation2() {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter the second number: ");
        double n2 = in.nextInt();
        return n2;

    }

    public static double division2 (double n1, double n2) throws ArithmeticException {

        double number = n1/n2;
        if (n2 == 0.0) {
            throw new ArithmeticException();
        }
        return number;
    }

}
