package salazar.w04;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
public class Customer {

    private String name;
    private long phone;
    private String address;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public String toString() {
        return "Name: " + name + " Phone: " + phone + " Address: " + address;
    }

    public static Customer JSONToCustomer(String s) {

        ObjectMapper mapper = new ObjectMapper();
        Customer customer = null;

        try {
            customer = mapper.readValue(s, Customer.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return customer;
    }
    public static String customerToJSON(Customer customer) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(customer);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }
}
