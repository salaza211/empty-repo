package salazar.w04;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;



public class server {
    public static String customerToJSON(Customer customer) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(customer);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/w04", new MyHandler());
        server.setExecutor(null);
        server.start();
        HTTPExample.getString();
        server.stop(1);
    }

    static class MyHandler implements HttpHandler {
        public void handle(HttpExchange t) throws IOException {
            Customer cust = new Customer();
            cust.setName("Harry");
            cust.setPhone(204940392);
            cust.setAddress("4 Privet Drive, Surrey");

            String json = Customer.customerToJSON(cust);
            byte [] response = json.getBytes();
            t.sendResponseHeaders(200, response.length);
            OutputStream os = t.getResponseBody();
            os.write(response);
            os.close();
        }


    }
}
