package salazar.w04;

import java.net.*;
import java.io.*;
import java.util.*;

public class HTTPExample {

    public static String getHttpContent(String string) {

        String content="";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            content = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return content;
    }

    public static Map getHttpHeaders(String string) {
        Map hashmap = null;

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            hashmap = http.getHeaderFields();

        } catch(Exception e) {
            System.err.println(e.toString());
        }

        return hashmap;

    }

    public static void getString() {
        System.out.println("This is what we started with on the server, a JSON file: " + HTTPExample.getHttpContent("http://localhost:8000/w04"));
        System.out.println("Then we change into the following string: ");
                Customer cust2 = test.JSONToCustomer(HTTPExample.getHttpContent("http://localhost:8000/w04"));
        System.out.println(cust2);

    }
}