import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String name = request.getParameter("name");
        String animal = request.getParameter("animal");
        String action = request.getParameter("action");
        out.println("<h1>A small story by you</h1>");
        out.println("<p>It all started on sweet summer moring, as  " + name + " was strolling down the forest.</p>");
        out.println("<p>Not far into the walk, a strange creature was seen. </p>");
        out.println("<p>It was a " + animal + ". </p>");
        out.println("<p>It looked menacing, but quiet.</p>");
        out.println("<p>As the creature was near the road, it was also getting closer.</p>");
        out.println("<p>At the closest point the " + animal + " asked: </p>");
        out.println("<p>-" + animal + ": Are you not late to " + action + "?</p>");
        out.println("<p>It was right, " + name + " thought. </p>");
        out.println("<p>" + name + " rushed to get out of its way and finish the day's business.</p>");
        out.println("<p>Never to see that " + animal + " again.</p>");
        out.println("</body></html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");

    }
}
