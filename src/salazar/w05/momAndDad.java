package salazar.w05;

public class momAndDad {
    private String[] mom;
    private String[] dad;

    public momAndDad(String[] mom, String[] dad) {
        this.mom = mom;
        this.dad = dad;
    }

    public String[] getDad() {
        return dad;
    }

    public String[] getMom() {
        return mom;
    }

    public void setDad(String[] dad) {
        this.dad = dad;
    }

    public void setMom(String[] mom) {
        this.mom = mom;
    }

    public boolean compareLists(String[] mom, String[] dad){
        int x = 0;
        boolean answer = true;
        while( x < mom.length && x < dad.length )
        {
            if (!dad[x].equals(mom[x])) {
                answer = false;
            }
            x++;
        }
        return answer;
    }
}
