package salazar.w05;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class momAndDadTest {


    @Test
    void getDad() {
        String[] mom = {""};
        String[] dad = {"beans", "soda", "corn"};
        try {
        momAndDad test1 = new momAndDad(mom, dad);
        }
        catch (Exception e){
            System.out.println("There was an error creating the class momAndDad.");
        }
        momAndDad test1 = new momAndDad(mom, dad);
        assertNotNull(test1.getDad());
        assertArrayEquals(dad, test1.getDad());
        assertSame(dad, test1.getDad());

    }

    @Test
    void getMom() {
        String[] mom = {"beans", "water", "corn"};
        String[] dad = {""};
        try {
            momAndDad test2 = new momAndDad(mom, dad);
        }
        catch (Exception e){
            System.out.println("There was an error creating a new class momAndDad.");
        }
        momAndDad test2 = new momAndDad(mom, dad);
        assertNotNull(test2.getDad());
        assertArrayEquals(mom, test2.getMom());
        assertSame(mom, test2.getMom());
    }

    @Test
    void compareLists() {
        String[] mom = {"beans", "water", "corn"};
        String[] dad = {"beans", "soda", "corn"};
        momAndDad test3 = new momAndDad(mom, dad);
        assertFalse(test3.compareLists(mom, dad));
    }


}