package salazar.w05;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CupTest {

    @Test
    void getLiquidType() {
        Cup c = new Cup("Bean Juice", 85.5);
        assertEquals("Bean Juice", c.getLiquidType());
    }
}