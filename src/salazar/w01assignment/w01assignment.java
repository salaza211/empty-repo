package salazar.w01assignment;
import salazar.basiccollections.DVDs;

import java.util.*;

public class w01assignment {
    public static void main(String[] args) {
        System.out.println("Today we will be taking a look at a list of groceries");
        System.out.println("for a family.");
        System.out.println("The products they have written down are: ");
        String[] groceries = {"Soda", "bread", "Corn", "Beans", "Tortillas", "Soda", "Candy"};
        System.out.println("Soda, bread, Corn, Beans, Tortillas, Soda, Candy");
        System.out.println();
        System.out.println("Take a look at the following examples and try to notice the " +
                "differences between collections");
        //List filling and printing
        System.out.println();
        System.out.println("If we use a LIST, we woudl get the follwoing results: ");
        List<String> listGroceries = new ArrayList();
        for (String x : groceries) {
            listGroceries.add(x);
        }
        System.out.println();

        for (Object str : listGroceries) {
            System.out.println((String) str);
        }

        //Set filling and printing
        System.out.println();
        System.out.println("If we use a SET, we would get the following results: ");
        Set setGroceries = new TreeSet();
        for (String x : groceries) {
            setGroceries.add(x);
        }
        System.out.println();
        for (Object str : setGroceries) {
            System.out.println((String)str);
        }

        //Queue filling and printing
        System.out.println();
        System.out.println("If we use a QUEUE, we would get the following results: ");
        Queue queueGroceries = new PriorityQueue();
        for (String x : groceries) {
            queueGroceries.add(x);
        }
        Iterator iterator = queueGroceries.iterator();
        while (iterator.hasNext()) {
            System.out.println(queueGroceries.poll());
        }

        System.out.println();
        System.out.println("If we use a MAP, we would get the following results: ");
        Map mapGroceries = new HashMap();
        int count = 1;
        for (String x : groceries) {
            mapGroceries.put(count, x);
            count++;
        }
        for (int i = 1; i < 8; i++) {
            String result = (String)mapGroceries.get(i);
            System.out.println(result);
        }

        System.out.println();
        System.out.println("Because it got rid of repeated groceries, we will choose SET in order to store" +
                "and sort our grocery list.");
        System.out.println();
        System.out.println("The mother would like to sort our grocery list by amount of product," +
                " in order to do this we will use an Amount Comprator." );
        System.out.println("This will let us sort our list, " +
                "even when items are being added in different order.");
        System.out.println();
        System.out.println("Now we will use a sorting system that will contain the following information " +
                "according to the family's needs");

        Set<Groceries> sortingList = new TreeSet<Groceries>(new AmountComparator());
        sortingList.add(new Groceries("Soda", 3));
        sortingList.add(new Groceries("bread", 4));
        sortingList.add(new Groceries("Corn", 5));
        sortingList.add(new Groceries("Beans", 3));
        sortingList.add(new Groceries("Tortillas", 6));
        sortingList.add(new Groceries("Soda", 3));
        sortingList.add(new Groceries("Candy", 3));

        System.out.println("Sorting by Amount");

        for(Groceries st: sortingList){
            System.out.println(st);
        }
    }
}