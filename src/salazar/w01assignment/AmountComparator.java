package salazar.w01assignment;

import java.util.*;

class AmountComparator implements Comparator<Groceries>{
    public int compare(Groceries s1,Groceries s2){
        if(s1.amount>s2.amount)
            return -1;
        else
            return 1;
    }
}