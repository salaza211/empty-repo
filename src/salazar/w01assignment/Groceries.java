package salazar.w01assignment;

class Groceries {
    String product;
    int amount;

    public Groceries(String product, int amount){
        this.product = product;
        this.amount = amount;
    }

    public String toString() {
        return "Product: " + product + "  ||  Amount: " + amount;
    }


}