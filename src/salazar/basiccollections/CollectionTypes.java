package salazar.basiccollections;

import java.util.*;

public class CollectionTypes {
    public static void main(String[] args) {

        System.out.println("---List---");
        List list = new ArrayList();
        list.add("Hi");
        list.add("my");
        list.add("name");
        list.add("is");
        list.add("The");
        list.add("One");
        for (Object str : list) {
            System.out.println((String)str);
        }

        System.out.println("---Set---");
        Set set = new TreeSet();
        set.add("Hi");
        set.add("my");
        set.add("name");
        set.add("is");
        set.add("The");
        set.add("Hi");
        for (Object str : set) {
            System.out.println((String)str);
        }

        System.out.println("---Queue---");
        Queue queue = new PriorityQueue();
        queue.add("Hi");
        queue.add("my");
        queue.add("name");
        queue.add("is");
        queue.add("The");
        queue.add("Hi");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("---Map--");
        Map map = new HashMap();
        map.put(1,"Hi");
        map.put(2,"my");
        map.put(3,"name");
        map.put(4,"is");
        map.put(5,"The");
        map.put(4,"Hi");

        for (int i = 1; i < 6; i++) {
            String result = (String)map.get(i);
            System.out.println(result);
        }

        System.out.println("---List Using Generics---");
        List<DVDs> myList = new LinkedList<DVDs>();
        myList.add(new DVDs( "Star Wars", "George Lucas" ));
        myList.add(new DVDs( "Star Wars", "George Lucas" ));

        for (DVDs dvd : myList) {
            System.out.println(dvd);
        }

    }
}
